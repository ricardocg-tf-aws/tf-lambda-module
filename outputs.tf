output "lambda_name" {
    value = aws_lambda_function.lambda.function_name
}

output "role" {
    value = var.use_iam_role
}