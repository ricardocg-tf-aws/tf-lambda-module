provider "aws" {
  region = "us-east-1"
}

# Enable cloud watch Monitoring - Logs
resource "aws_cloudwatch_log_group" "example" {
  name              = "/aws/lambda/${var.name}"
  retention_in_days = 5
}

#AWS managed policy: AWSLambdaBasicExecutionRole
resource "aws_iam_role" "iam_for_lambda" {
  count               = "${var.use_iam_role ? 0 : 1}"
  name                = "iam_for_lambda"

  assume_role_policy  = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

#attach user supplied policy 
resource "aws_iam_policy" "user_supplied_policy" {
  count      = var.use_iam_role ? 0 : 1
  policy     = var.iam_policy_doc
  path       = "/"
}
resource "aws_iam_role_policy_attachment" "user_supplied_policy" {
  count      = var.use_iam_role ? 0 : 1
  role       = aws_iam_role.iam_for_lambda[0].name
  policy_arn = aws_iam_policy.user_supplied_policy[0].arn
}

# Lambda loggin policy 
resource "aws_iam_policy" "lambda_logging" {
  count       = "${var.use_iam_role ? 0 : 1}"
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_lambda_function" "lambda" {
  description       = var.description
  filename          = var.filename
  function_name     = var.name
  role              = var.use_iam_role ? var.created_role_arn : aws_iam_role.iam_for_lambda[0].arn
  handler           = var.handler
  runtime           = var.runtime
  depends_on        = ["aws_iam_role_policy_attachment.lambda_logs", "aws_cloudwatch_log_group.example"]

  environment {
    variables = {
      env = var.env
    }
  }
}

#Log attachment
resource "aws_iam_role_policy_attachment" "lambda_logs" {
  count      = "${var.use_iam_role ? 0 : 1}"
  role       = "${aws_iam_role.iam_for_lambda[0].name}"
  policy_arn = "${aws_iam_policy.lambda_logging[0].arn}"
}
