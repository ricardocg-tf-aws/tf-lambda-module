# AWS Lambda Module

-  Module to deploy basic lambda function

# Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| Name | Name for the function | String | - |:yes:|
| Description | Brief description| String |  |:no:|
| Env | Env ehere the vpc should be deployed  | String | - |:yes:|
| Filename | Path to the script name| String | - |:yes:|
| Handler | Handler for the function | String | - |:yes:|
| Runtime | Runtime for the function i.e python37 | String | - |:yes:|
| Use existing iam Role | If set to true no default policy is created | Boolean | false |:no:|
| created_role_arn | Role to assume | String | - |:no:|
| iam_policy_doc | User provide IAM Policy to attach, data resources is required if dont submit a custom role | String | - |: no :|


# Outputs

| Name | Description |
|------|-------------|
| Name | Lambda function name|

# Usage

```js
module "lambda" {
    source                          = "../../"
    name                            = "Lambda-test"
    description                     = "Test"
    filename                        = "./src/lambda/lambda.zip"
    handler                         = "lambda_function.lambda_handler"
    runtime                         = "python3.7"
    env                             = "Test"
    

}
```
