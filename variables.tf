variable "name" {
    description     = "Name for the lambda Function"
    type            = "string"
}

variable "description" {
    description     = "General description"
    type            = "string"
    default         = ""
}

variable "filename" {
    description     = "Name of the script - full PATH"
    type            = "string"
}

variable "handler" {
    description     = "lambda handler | entrypoint function"
    type            = "string"
}

variable "runtime" {
    description     = "runtime i.e. python | javascript | etc"
    type            = "string"
}

variable "env" {
    description     = "Environment"
    type            = "string"
}

variable "use_iam_role" {
    description         = "Create or not a defualt role and policy if set to true dont create default in module"
    default             = false
}

variable "created_role_arn" {
    description         = "If defined, this si the IAM role the lambda will assume instead of default"
    default             = ""
}

variable "iam_policy_doc" {
    description         = "user provided custom policy for lambda execution / needs to be a data resource"
    default             = null
}